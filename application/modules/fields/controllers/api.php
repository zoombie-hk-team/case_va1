<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	private $_httpBadRequestStatusCode = 400;

	function __construct()
	{
		parent::__construct();

		$this->load->model('fields_model');
		$this->r = array('status' => 200, 'message' => 'error');
	}

	public function index_post()
	{
		$postData = $_POST;
//		$postData = json_decode($this->request->body,true);

		$wheres = [];
		$columns = $postData['columns'];
		foreach ($columns as $column) {
			if ($column['searchable'] && !empty($column['search']['value'])) {
				$wheres[$column['data']] = $column['search']['value'];
			}
		}

		$wheres = array_merge($wheres, [
			'limit' => isset($postData['length']) ? $postData['length'] : 10,
			'skip'  => isset($postData['start']) ? $postData['start'] : 0
		]);
		$list = $this->fields_model->lists($wheres);

		$responseData = [];
		foreach ($list as $row) {
			$row['_id'] = (string)$row['_id'];
			$responseData[] = $row;
		}

		$this->r['message'] = 'success';
		$this->r['data'] = $responseData;

		// pagination
		$this->r['draw'] = isset($postData['draw']) ? $postData['draw'] : null;
		$this->r['iTotalRecords'] = $this->fields_model->count();
		$this->r['iTotalDisplayRecords'] = $this->fields_model->countByQueries();

		$this->r['data'] = $responseData;
		$this->response($this->r);
	}

	public function create_post()
	{
		$postData = $_POST;
//		$postData = json_decode($this->request->body,true);
		$insertData = $this->_validatePost($postData);
		$_id = $this->fields_model->create($insertData);

		$this->r['data'] = [
			'_id' => (string)$_id
		];
		$this->r['message'] = 'success';
		$this->response($this->r);
	}

	public function edit_post()
	{
		$postData = $_POST;
//		$postData = json_decode($this->request->body,true);

		if (!isset($postData['_id']) && empty($postData['_id'])) {
			show_error("Missing field _id in post data", $this->_httpBadRequestStatusCode);
		}

		$_id = $postData['_id'];
		$this->fields_model->findOneOrFail($_id);

		$data = $this->_validatePost($postData);
		$this->fields_model->update($_id , $data);

		$this->r['message'] = 'success';
		$this->response($this->r);
	}

	public function del_post()
	{
		$postData = $_POST;
//		$postData = json_decode($this->request->body,true);
		if (!isset($postData['_id']) && empty($postData['_id'])) {
			show_error("Missing field _id in post data", $this->_httpBadRequestStatusCode);
		}

		$_id = $postData['_id'];
		$row = $this->fields_model->findOneOrFail($_id);
		if ($row['system']) {
			show_error("Không thể xóa field system");
		}

		$this->fields_model->del($_id);

		$this->r['message'] = 'success';
		$this->response($this->r);
	}

	private function _validatePost($requestData = array())
	{
		$name = $this->_validateName($requestData);
		$label = $this->_validateLabel($requestData);
		$type = $this->_validateType($requestData);
		$required = $this->_validateRequired($requestData);
		$display = $this->_validateDisplay($requestData);
		$filter = $this->_validateFilter($requestData);
		$displaySort = $this->_validateDisplaySort($requestData);
		$filterSort = $this->_validateFilterSort($requestData);
		$clientId = $this->_validateClient($requestData);
		$moduleName = $this->_validateModuleName($requestData);
		$system = $this->_validateSystem($requestData);
		$data = $this->_validateData($requestData);

		return [
			'name' => $name,
			'label' => $label,
			'type' => $type,
			'required' => $required,
			'display' => $display,
			'filter' => $filter,
			'display_sort' => $displaySort,
			'filter_sort' => $filterSort,
			'client_id' => $clientId,
			'module_name' => $moduleName,
			'system' => $system,
			'data' => $data,
		];
	}

	private function _validateName($requestData)
	{
		if (!isset($requestData['name']) || empty($requestData['name'])) {
			show_error("Trường Tên đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!isset($requestData['module_name']) || empty($requestData['module_name'])) {
			show_error("Trường Module đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!isset($requestData['client_id']) || empty($requestData['client_id'])) {
			show_error("Trường Client đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		$name = $requestData['name'];

		// Validate format
		if (!preg_match("/^[a-zA-Z ]*/", $name)) {
			show_error("Trường Tên phải không chứa ký tự số, dấu, ký tự đặc biệt", 400);
		}

		// Auto format: ho_va_ten
		$name = strtolower(trim($name));
		$name = preg_replace("/( +)/", '_', $name);

		// Validate unique: module-client-name
		$this->fields_model->validateNameUnique($name, $requestData);

		return $name;
	}

	private function _validateLabel($requestData)
	{
		if (!isset($requestData['label']) || empty($requestData['label'])) {
			show_error("Trường Label đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (strlen($requestData['label']) < 3 || strlen($requestData['label']) > 32) {
			show_error("Trường Label có độ dài từ 3 đến 32 ký tự", $this->_httpBadRequestStatusCode);
		}

		return $requestData['label'];
	}

	private function _validateType($requestData)
	{
		if (!isset($requestData['type']) || empty($requestData['type'])) {
			show_error("Trường Type đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!in_array($requestData['type'], ['string','int','date','select','text','textsort'])) {
			show_error('Trường Type không hợp lệ');
		}

		return $requestData['type'];
	}

	private function _validateRequired($requestData)
	{
		if (!isset($requestData['required'])) {
			show_error("Trường Required đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!is_bool($requestData['required'])) {
			show_error('Trường required không hợp lệ');
		}

		return $requestData['required'];
	}

	private function _validateDisplay($requestData)
	{
		if (!isset($requestData['display'])) {
			show_error("Trường Display đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!is_bool($requestData['display'])) {
			show_error('Trường Display không hợp lệ');
		}

		return $requestData['display'];
	}

	private function _validateFilter($requestData)
	{
		if (!isset($requestData['filter'])) {
			show_error("Trường Filter đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!is_bool($requestData['filter'])) {
			show_error('Trường Filter không hợp lệ');
		}

		return $requestData['filter'];
	}

	private function _validateDisplaySort($requestData)
	{
		if (!isset($requestData['display_sort']) || empty($requestData['display_sort'])) {
			show_error("Trường Display Sort đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		$this->fields_model->validateDisplaySort($requestData['display_sort'], [
			'module_name' => $requestData['module_name']
		]);

		return $requestData['display_sort'];
	}

	private function _validateFilterSort($requestData)
	{
		if (!isset($requestData['filter_sort']) || empty($requestData['filter_sort'])) {
			show_error("Trường Filter Sort đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		$this->fields_model->_validateFilterSort($requestData['filter_sort'], [
			'module_name' => $requestData['module_name']
		]);

		return $requestData['filter_sort'];
	}

	private function _validateClient($requestData)
	{
		if (!isset($requestData['client_id']) || empty($requestData['client_id'])) {
			show_error("Trường Client đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		return $requestData['client_id'];
	}

	private function _validateModuleName($requestData)
	{
		if (!isset($requestData['module_name']) || empty($requestData['module_name'])) {
			show_error("Trường Module đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!in_array($requestData['module_name'], ['contact','contract','case','billing'])) {
			show_error('Trường Module không hợp lệ');
		}

		return $requestData['module_name'];
	}

	private function _validateSystem($requestData)
	{
		if (!isset($requestData['system'])) {
			show_error("Trường System đang bỏ trống", $this->_httpBadRequestStatusCode);
		}

		if (!is_bool($requestData['system'])) {
			show_error('Trường System không hợp lệ');
		}

		return $requestData['system'];
	}

	private function _validateData($requestData)
	{
		if (isset($requestData['data']) && !empty($requestData['data']) &&
			$requestData['type'] == 'select' && !is_array($requestData['data'])
		) {
			show_error("Trường data của kiểu select phải là một mảng", $this->_httpBadRequestStatusCode);
		}

		if ($requestData['type'] == 'select' && empty($requestData['data'])) {
			$requestData['data'] = [];
		}

		return $requestData['data'];
	}
}

?>
