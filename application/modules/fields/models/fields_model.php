<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class fields_model extends CI_Model
{
	public $collection = 'fields';

	public function __construct()
	{
		parent::__construct();
	}
	
	/*
	    Trong Model ngoài 4 function trên không có bất kỳ function dù public hay private nào cả,
	    list,del,update,create
	    
	    public function list(){}
	     public function del(){}
	      public function update(){}
	       public function create(){}
	*/
	

	public function lists($where = array())
	{
		$this->mongo_db->limit($where['limit']);
		$this->mongo_db->offset($where['skip']);

		unset($where['limit']);
		unset($where['skip']);

		return $this->mongo_db->where($where)->get($this->collection);
	}

	/**
	 * Insert document
	 * Return new inserted id
	 *
	 * @param $data
	 * @return mixed
	 */
	public function create($data)
	{
		$insertedData = [
			'name' => (string)$data['name'],
			'label' => (string)$data['label'],
			'type' => (string)$data['type'],
			'required' => (boolean)$data['required'],
			'display' => (boolean)$data['display'],
			'fillter' => (boolean)$data['filter'],
			'display_sort' => (int)$data['display_sort'],
			'filter_sort' => (int)$data['filter_sort'],
			'client_id' => (string)$data['client_id'],
			'module_name' => (string)$data['module_name'],
			'system' => (boolean)$data['system'],
			'data' => $this->_forceData($data['data'], $data['type']),
		];

		return $this->mongo_db->insert($this->collection, $insertedData);
	}

	private function _forceData($data, $type)
	{
		if (isset($data)) {
			if ($type == 'select') {
				$data = array($data);
			} else if ($type == 'int') {
				$data = (int)$data;
			} else if (in_array($type, ['string', 'text', 'textsort'])) {
				$data = (string)$data;
			} else if ($data == 'date') {
				$data = new \MongoDB\BSON\UTCDateTime(strtotime(date($data)) * 1000);
			}
		} else {
			if ($type == 'select') {
				$data = array();
			} else {
				$data = null;
			}
		}

		return $data;
	}

	/**
	 * Update document
	 *
	 * @param $_id
	 * @param $data
	 * @return mixed
	 */
	public function update($_id, $data)
	{
		$updatedData = [
			'name' => (string)$data['name'],
			'label' => (string)$data['label'],
			'type' => (string)$data['type'],
			'required' => (boolean)$data['required'],
			'display_sort' => (int)$data['display_sort'],
			'filter_sort' => (int)$data['filter_sort'],
			'module_name' => (string)$data['module_name'],
			'data' => $this->_forceData($data['data'], $data['type']),
		];

		$row = $this->mongo_db->where([
			'_id' => new MongoId($_id)
		])
			->find_one($this->collection)
		;
		if (!$row['system']) {
			$updatedData['system'] = $row['system'];
		}

		$where = [
			'_id' => new MongoId($_id)
		];
		return $this->mongo_db->where($where)
			->set($updatedData)
			->update($this->collection)
		;
	}

	public function countByQueries($where = [])
	{
		return $this->mongo_db->where($where)->count($this->collection);
	}

	public function validateNameUnique($name, $where = null)
	{
		if (isset($where['_id'])) {
			$this->mongo_db->where_ne('_id', new MongoId($where['_id']));
			unset($where['_id']);
		}

		$where = array_merge($where, [
			'name' => $name,
			'module_name' => $where['module_name'],
			'client_id' => $where['client_id'],
		]);
		$result = $this->mongo_db->where($where)->get($this->collection);
		if ($result) {
			show_error( 'Module đã có client và tên field này rồi', 400);
		}
	}

	public function validateDisplaySort($displaySort, $where = null)
	{
		$max = $this->countByQueries($where) + 1;
		if ($displaySort > $max) {
			show_error("Maximum display sort is $max", 400);
		}
	}

	public function _validateFilterSort($filterSort, $where = null)
	{
		$max = $this->countByQueries($where) + 1;
		if ($filterSort > $max) {
			show_error("Maximum filter sort is $max", 400);
		}
	}

	/**
	 * Find by _Id.
	 * Die if not exit
	 *
	 * @param $id
	 * @param $where
	 * @return mixed
	 */
	public function findOneOrFail($id, $where = array())
	{
		$where = array_merge($where, [
			'_id' => new MongoId($id)
		]);
		$row = $this->mongo_db->where($where)->find_one($this->collection);
		if (!$row) {
			show_error('Document not found');
		}

		return $row;
	}

	public function count()
	{
		return $this->mongo_db->count($this->collection);
	}

	/**
	 * Delete document
	 *
	 * @param $_id
	 * @param $where
	 * @return mixed
	 */
	public function del($_id, $where = array())
	{
		$where = array_merge($where, [
			'_id' => new MongoId($_id)
		]);

		return $this->mongo_db->where($where)->delete($this->collection);
	}
}

?>
